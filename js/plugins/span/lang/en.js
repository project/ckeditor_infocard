/**
 * @file
 * The InfoCard language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('span', 'en', {
  buttonTitle: 'Insert InfoCard',
  menuItemTitle: 'Edit InfoCard',
  dialogTitle: 'InfoCard Properties',
  dialogInfoCardTitle: 'InfoCard',
  dialogExplanationTitle: 'Explanation'
});
