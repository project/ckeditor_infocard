/**
 * @file
 * The InfoCard language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('span', 'de', {
  buttonTitle: 'InfoCard einfügen',
  menuItemTitle: 'InfoCard bearbeiten',
  dialogTitle: 'Eigenschaften InfoCard',
  dialogInfoCardTitle: 'InfoCard',
  dialogExplanationTitle: 'Erklärung'
});
