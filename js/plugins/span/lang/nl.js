/**
 * @file
 * The InfoCard language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('span', 'nl', {
  buttonTitle: 'InfoCard invoegen',
  menuItemTitle: 'InfoCard bewerken',
  dialogTitle: 'Eigenschappen InfoCard',
  dialogInfoCardTitle: 'InfoCard',
  dialogExplanationTitle: 'Text'
});
