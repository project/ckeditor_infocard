import { Plugin } from 'ckeditor5/src/core';
import InfoCardCommand from './infocardcommand';
import RemoveInfoCardCommand from './removeinfocardcommand';

export default class InfoCardEditing extends Plugin {
	init() {
		this._defineSchema();
		this._defineConverters();

		this.editor.commands.add(
			'addInfoCard', new InfoCardCommand( this.editor )
		);
    this.editor.commands.add(
      'removeInfoCard', new RemoveInfoCardCommand( this.editor )
    );
	}
	_defineSchema() {
		const schema = this.editor.model.schema;

    // Extend the text node's schema to accept the abbreviation attribute.
		schema.extend( '$text', {
			allowAttributes: [ 'infoCard' ]
		} );
	}
	_defineConverters() {
		const conversion = this.editor.conversion;

    // Conversion from a model attribute to a view element
		conversion.for( 'downcast' ).attributeToElement( {
			model: 'infoCard',

      // Callback function provides access to the model attribute value
			// and the DowncastWriter
			view: ( modelAttributeValue, conversionApi ) => {
				const { writer } = conversionApi;
        let dataContentAttribute = (modelAttributeValue)
          ? {
              "data-content": modelAttributeValue,
              "class": "js-infoCard"
            }
          : null;
        return writer.createAttributeElement('span', dataContentAttribute);
			}
		} );

		// Conversion from a view element to a model attribute
		conversion.for( 'upcast' ).elementToAttribute( {
			view: {
				name: 'span',
				attributes: [ 'data-content'],
        classes: 'js-infoCard'
			},
			model: {
				key: 'infoCard',

                // Callback function provides access to the view element
				value: viewElement => {
					const dataContent = viewElement.getAttribute( 'data-content' );
					return dataContent;
				}
			}
		} );
	}
}
