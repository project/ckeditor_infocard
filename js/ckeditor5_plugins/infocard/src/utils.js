/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */
import TextareaView from "./TextareaView";

// A helper function that retrieves and concatenates all text within the model range.
function getRangeText( range ) {
	return Array.from( range.getItems() ).reduce( ( rangeText, node ) => {
		if ( !( node.is( 'text' ) || node.is( 'textProxy' ) ) ) {
			return rangeText;
		}

		return rangeText + node.data;
	}, '' );
}

/**
 * The createLabeledTextarea function.
 *
 * Exported from CKEditor version 41.4.2 to work on version 39.0.1.
 * Location: ckeditor-ui/src/labeledfield/labeledfieldview.js
 *
 * Used to create a textarea with a label on top of it.
 *
 * No changes to source.
 *
 */
function createLabeledTextarea(labeledFieldView, viewUid, statusUid) {
  const textareaView = new TextareaView(labeledFieldView.locale);
  textareaView.set({
    id: viewUid,
    ariaDescribedById: statusUid
  });
  textareaView.bind('isReadOnly').to(labeledFieldView, 'isEnabled', value => !value);
  textareaView.bind('hasError').to(labeledFieldView, 'errorText', value => !!value);
  textareaView.on('input', () => {
    // UX: Make the error text disappear and disable the error indicator as the user
    // starts fixing the errors.
    labeledFieldView.errorText = null;
  });
  labeledFieldView.bind('isEmpty', 'isFocused', 'placeholder').to(textareaView);
  return textareaView;
}

export { getRangeText, createLabeledTextarea }
