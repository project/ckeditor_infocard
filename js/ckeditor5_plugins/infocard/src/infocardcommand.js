import { Command } from 'ckeditor5/src/core';
import findAttributeRange from '@ckeditor/ckeditor5-typing/src/utils/findattributerange';
import { getRangeText } from "./utils.js";
import { toMap } from 'ckeditor5/src/utils';

export default class InfoCardCommand extends Command {
    refresh() {
		const model = this.editor.model;
		const selection = model.document.selection;
		const firstRange = selection.getFirstRange();

		// When the selection is collapsed, the command has a value if the caret is in an infoCard.
		if ( firstRange.isCollapsed ) {
			if ( selection.hasAttribute( 'infoCard' ) ) {
				const attributeValue = selection.getAttribute( 'infoCard' );

				// Find the entire range containing the infoCard under the caret position.
				const infoCardRange = findAttributeRange( selection.getFirstPosition(), 'infoCard', attributeValue, model );

				this.value = {
					span: getRangeText( infoCardRange ),
					dataContent: attributeValue,
					range: infoCardRange
				};
			} else {
				this.value = null;
			}
		}
		// When the selection is not collapsed, the command has a value if the selection contains a subset of a single infoCard
		// or an entire infoCard.
		else {
			if ( selection.hasAttribute( 'infoCard' ) ) {
				const attributeValue = selection.getAttribute( 'infoCard' );

				// Find the entire range containing the infoCard under the caret position.
				const infoCardRange = findAttributeRange( selection.getFirstPosition(), 'infoCard', attributeValue, model );

				if ( infoCardRange.containsRange( firstRange, true ) ) {
					this.value = {
            infoCard: getRangeText( firstRange ),
						dataContent: attributeValue,
						range: firstRange
					};
          // console.log( this.value.span );
				} else {
					this.value = null;
				}
			} else {
				this.value = null;
			}
		}

		// The command is enabled when the "infoCard" attribute can be set on the current model selection.
		this.isEnabled = model.schema.checkAttributeInSelection( selection, 'infoCard' );
	}

	execute( { infoCard, dataContent } ) {
		const model = this.editor.model;
		const selection = model.document.selection;

		model.change( writer => {
			// If selection is collapsed then update the selected infoCard or insert a new one at the place of caret.
			if ( selection.isCollapsed ) {
				// When a collapsed selection is inside text with the "infoCard" attribute, update its text and dataContent.
				if ( this.value ) {
					const { end: positionAfter } = model.insertContent(
						writer.createText( infoCard, { infoCard: dataContent } ),
						this.value.range
					);
					// Put the selection at the end of the inserted infoCard.
					writer.setSelection( positionAfter );
				}
				// If the collapsed selection is not in an existing infoCard, insert a text node with the "infoCard" attribute
				// in place of the caret. Because the selection is collapsed, the attribute value will be used as a data for text.
				// If the infoCard is empty, do not do anything.
				else if ( infoCard !== '' ) {
					const firstPosition = selection.getFirstPosition();

					// Collect all attributes of the user selection (could be "bold", "italic", etc.)
					const attributes = toMap( selection.getAttributes() );

					// Put the new attribute to the map of attributes.
					attributes.set( 'infoCard', dataContent );

					// Inject the new text node with the abbreviation text with all selection attributes.
					const { end: positionAfter } = model.insertContent( writer.createText( infoCard, attributes ), firstPosition );

					// Put the selection at the end of the inserted infoCard. Using an end of a range returned from
					// insertContent() just in case nodes with the same attributes were merged.
					writer.setSelection( positionAfter );
				}

				// Remove the "infoCard" attribute attribute from the selection. It stops adding a new content into the infoCard
				// if the user starts to type.
				writer.removeSelectionAttribute( 'infoCard' );
			} else {
				// If the selection has non-collapsed ranges, change the attribute on nodes inside those ranges
				// omitting nodes where the "infoCard" attribute is disallowed.
				const ranges = model.schema.getValidRanges( selection.getRanges(), 'infoCard' );

				for ( const range of ranges ) {
					writer.setAttribute( 'infoCard', dataContent, range );
				}
			}
		} );
	}
}
