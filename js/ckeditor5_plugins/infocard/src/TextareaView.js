/**
 * @license Copyright (c) 2003-2024, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
/**
 * @module ui/textarea/textareaview
 */

import { InputView } from "ckeditor5/src/ui";
import { CKEditorError, global, isVisible, Rect, ResizeObserver, toUnit } from "ckeditor5/src/utils";

/**
 * The textarea view class.
 *
 * Exported from CKEditor version 41.4.2 to work on version 39.0.1.
 * Location: ckeditor-ui/src/textarea/TextareaView.js
 * Changes to this class are as follows:
 * Using extend InputView instead of InputBase (because the latter is unavailable in this version)
 * Also getBorderWidths is unavailable in this version, hence references to it are removed from the class
 * Also added a default-width of 10000
 * Also changed resize to vertical by default (instead of 'none
 *
 * ```ts
 * const textareaView = new TextareaView();
 *
 * textareaView.minRows = 2;
 * textareaView.maxRows = 10;
 *
 * textareaView.render();
 *
 * document.body.append( textareaView.element );
 * ```
 */
export default class TextareaView extends InputView {
  /**
   * @inheritDoc
   */
  constructor(locale) {
    super(locale);
    /**
     * A flag that indicates whether the {@link #_updateAutoGrowHeight} method should be called when the view becomes
     * visible again. See {@link #_resizeObserver}.
     */
    this._isUpdateAutoGrowHeightPending = false;
    const toPx = toUnit('px');
    this.set('minRows', 5);
    this.set('maxRows', 5);
    this.set('_height', null);
    this.set('_width', 1000);
    this.set('resize', 'vertical');
    this._resizeObserver = null;
    this.on('change:minRows', this._validateMinMaxRows.bind(this));
    this.on('change:maxRows', this._validateMinMaxRows.bind(this));
    const bind = this.bindTemplate;
    this.template.tag = 'textarea';
    this.extendTemplate({
      attributes: {
        class: [ 'ck-textarea' ],
        id: 'infocard_datacontent_textarea',
        style: {
          height: bind.to('_height', height => height ? toPx(height) : null),
          width: bind.to('_width', width => width ? toPx(width) : null),
          resize: bind.to('resize')
        },
        rows: bind.to('minRows'),
      }
    });
  }
  /**
   *   {
   *             tag: 'textarea',
   *             class: [
   *               'form-textarea',
   *               'resize-vertical',
   *               'required',
   *               'form-element',
   *               'form-element--type-textarea',
   *               'form-element--api-textarea',
   *             ]
   *           }
   * @inheritDoc
   */
  render() {
    super.render();
    let wasVisible = false;
    this.on('input', () => {
      this._updateAutoGrowHeight(true);
      this.fire('update');
    });
    this.on('change:value', () => {
      // The content needs to be updated by the browser after the value is changed. It takes a few ms.
      global.window.requestAnimationFrame(() => {
        if (!isVisible(this.element)) {
          this._isUpdateAutoGrowHeightPending = true;
          return;
        }
        this._updateAutoGrowHeight();
        this.fire('update');
      });
    });
    // It may occur that the Textarea size needs to be updated (e.g. because it's content was changed)
    // when it is not visible or detached from DOM.
    // In such case, we need to detect the moment when it becomes visible again and update its height then.
    // We're using ResizeObserver for that as it is the most reliable way to detect when the element becomes visible.
    // IntersectionObserver didn't work well with the absolute positioned containers.
    this._resizeObserver = new ResizeObserver(this.element, evt => {
      const isVisible = !!evt.contentRect.width && !!evt.contentRect.height;
      if (!wasVisible && isVisible && this._isUpdateAutoGrowHeightPending) {
        // We're wrapping the auto-grow logic in RAF because otherwise there is an error thrown
        // by the browser about recursive calls to the ResizeObserver. It used to happen in unit
        // tests only, though. Since there is no risk of infinite loop here, it can stay here.
        global.window.requestAnimationFrame(() => {
          this._updateAutoGrowHeight();
          this.fire('update');
        });
      }
      wasVisible = isVisible;
    });
  }
  /**
   * @inheritDoc
   */
  destroy() {
    if (this._resizeObserver) {
      this._resizeObserver.destroy();
    }
  }
  /**
   * @inheritDoc
   */
  reset() {
    super.reset();
    this._updateAutoGrowHeight();
    this.fire('update');
  }
  /**
   * Updates the {@link #_height} of the view depending on {@link #minRows}, {@link #maxRows}, and the current content size.
   *
   * **Note**: This method overrides manual resize done by the user using a handle. It's a known bug.
   */
  _updateAutoGrowHeight(shouldScroll) {
    const viewElement = this.element;
    if (!viewElement.offsetParent) {
      this._isUpdateAutoGrowHeightPending = true;
      return;
    }
    this._isUpdateAutoGrowHeightPending = false;
    const singleLineContentClone = getTextareaElementClone(viewElement, '1');
    const fullTextValueClone = getTextareaElementClone(viewElement, viewElement.value);
    const singleLineContentStyles = singleLineContentClone.ownerDocument.defaultView.getComputedStyle(singleLineContentClone);
    const verticalPaddings = parseFloat(singleLineContentStyles.paddingTop) + parseFloat(singleLineContentStyles.paddingBottom);
    const lineHeight = parseFloat(singleLineContentStyles.lineHeight);
    const singleLineAreaDefaultHeight = new Rect(singleLineContentClone).height;
    const numberOfLines = Math.round((fullTextValueClone.scrollHeight - verticalPaddings) / lineHeight);
    // There's a --ck-ui-component-min-height CSS custom property that enforces min height of the component.
    // This min-height is relevant only when there's one line of text. Other than that, we can rely on line-height.
    const minHeight = numberOfLines === 1 ? singleLineAreaDefaultHeight : this.minRows * lineHeight + verticalPaddings + verticalBorder;
    if (shouldScroll) {
      viewElement.scrollTop = viewElement.scrollHeight;
    }
    singleLineContentClone.remove();
    fullTextValueClone.remove();
  }
  /**
   * Validates the {@link #minRows} and {@link #maxRows} properties and warns in the console if the configuration is incorrect.
   */
  _validateMinMaxRows() {
    if (this.minRows > this.maxRows) {
      /**
       * The minimum number of rows is greater than the maximum number of rows.
       *
       * @error ui-textarea-view-min-rows-greater-than-max-rows
       * @param textareaView The misconfigured textarea view instance.
       * @param minRows The value of `minRows` property.
       * @param maxRows The value of `maxRows` property.
       */
      throw new CKEditorError('ui-textarea-view-min-rows-greater-than-max-rows', {
        textareaView: this,
        minRows: this.minRows,
        maxRows: this.maxRows
      });
    }
  }
}
function getTextareaElementClone(element, value) {
  const clone = element.cloneNode();
  clone.style.position = 'absolute';
  clone.style.top = '-99999px';
  clone.style.left = '-99999px';
  clone.style.height = 'auto';
  clone.style.overflow = 'hidden';
  clone.style.width = element.ownerDocument.defaultView.getComputedStyle(element).width;
  clone.tabIndex = -1;
  clone.rows = 1;
  clone.value = value;
  element.parentNode.insertBefore(clone, element);
  return clone;
}
