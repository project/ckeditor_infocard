import { Plugin } from "ckeditor5/src/core";
import { ButtonView, clickOutsideHandler, ContextualBalloon } from "ckeditor5/src/ui";
import FormView from "./infocardview";
import { getRangeText } from "./utils.js";

import infoCardIcon from "../../../../icons/infoCard.svg";

export default class InfoCardUI extends Plugin {
	static get requires() {
		return [ ContextualBalloon ];
	}

	init() {
		const editor = this.editor;

    // Create the balloon and the form view.
		this._balloon = this.editor.plugins.get( ContextualBalloon );
		this.formView = this._createFormView();
		const addInfoCardCommand = editor.commands.get('addInfoCard');

		editor.ui.componentFactory.add( 'infoCard', (locale) => {
			const button = new ButtonView(locale);

			button.set({
				label: 'InfoCard',
				tooltip: true,
				icon: infoCardIcon,
			});

			// Show the UI on button click.
			this.listenTo( button, 'execute', () => {
				this._showUI();
			} );

			// Bind the state of the button to the command.
			button.bind('isOn', 'isEnabled').to(addInfoCardCommand, 'value', 'isEnabled');

			return button;
		} );
	}

	_createFormView() {
		const editor = this.editor;
		const formView = new FormView( editor.locale );

		// Execute the command after clicking the "Save" button.
		this.listenTo( formView, 'submit', () => {
			// Grab values from the dataContent and span input fields.
			const value = {
        infoCard: formView.infoCardHintInputView.fieldView.element.value,
				dataContent: formView.infoCardDataContentInputView.fieldView.element.value
			};
			editor.execute( 'addInfoCard', value );

      // Hide the form view after submit.
			this._hideUI();
		} );

    // Remove the span tag after clicking the "Remove" button.
    this.listenTo( formView, 'remove', () => {
    	editor.execute( 'removeInfoCard' );
    	this._hideUI();
    } );

		// Hide the form view after clicking the "Cancel" button.
		this.listenTo( formView, 'cancel', () => {
			this._hideUI();
		} );

		// Hide the form view when clicking outside the balloon.
		clickOutsideHandler( {
			emitter: formView,
			activator: () => this._balloon.visibleView === formView,
			contextElements: [ this._balloon.view.element ],
			callback: () => this._hideUI()
		} );

		return formView;
	}

	_showUI() {
		const selection = this.editor.model.document.selection;

		// Check the value of the command.
		const commandValue = this.editor.commands.get( 'addInfoCard' ).value;

		this._balloon.add( {
			view: this.formView,
			position: this._getBalloonPositionData()
		} );

    // Enable the remove button when the value of the command is not null.
    this.formView.removeButtonView.isEnabled = !!commandValue;

		// Disable the input when the selection is not collapsed.
		this.formView.infoCardHintInputView.isEnabled = selection.getFirstRange().isCollapsed;

		// Fill the form using the state (value) of the command.
		if ( commandValue ) {
			this.formView.infoCardHintInputView.fieldView.value = commandValue.infoCard;
			this.formView.infoCardDataContentInputView.fieldView.value = commandValue.dataContent;
		}
		// If the command has no value, put the currently selected text (not collapsed)
		// in the first field and empty the second in that case.
		else {
      this.formView.infoCardHintInputView.fieldView.value = getRangeText(selection.getFirstRange());
			this.formView.infoCardDataContentInputView.fieldView.value = '';
		}

		this.formView.focus();
	}

	_hideUI() {
		// Clear the input field values and reset the form.
		this.formView.infoCardHintInputView.fieldView.value = '';
		this.formView.infoCardDataContentInputView.fieldView.value = '';
		this.formView.element.reset();

		this._balloon.remove( this.formView );

		// Focus the editing view after inserting the infoCard so the user can start typing the content
		// right away and keep the editor focused.
		this.editor.editing.view.focus();
	}

	_getBalloonPositionData() {
		const view = this.editor.editing.view;
		const viewDocument = view.document;
		let target = null;

		// Set a target position by converting view selection range to DOM
		target = () => view.domConverter.viewRangeToDom( viewDocument.selection.getFirstRange() );

		return {
			target
		};
	}
}
