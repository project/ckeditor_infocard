import {
	View,
	LabeledFieldView,
	createLabeledInputText,
	ButtonView,
	submitHandler,
	FocusCycler,
} from 'ckeditor5/src/ui';

import { FocusTracker, KeystrokeHandler } from 'ckeditor5/src/utils';
import { icons } from 'ckeditor5/src/core';
import { createLabeledTextarea } from "./utils";

export default class FormView extends View {
	constructor( locale ) {
		super( locale );

		this.focusTracker = new FocusTracker();
		this.keystrokes = new KeystrokeHandler();

		this.infoCardHintInputView = this._createInput(  'InfoCard' );

    this.infoCardDataContentInputView = new LabeledFieldView( this.locale,  createLabeledTextarea );

    this.infoCardDataContentInputView.label = 'Add content to info card';
    // Drupal.editors.ckeditor5.attach(this.infoCardDataContentInputView, 'Infocard')
    this.saveButtonView = this._createButton( Drupal.t('Save'), icons.check, 'ck-button-save' );

		// Submit type of the button will trigger the submit event on entire form when clicked
		this.saveButtonView.type = 'submit';

		this.cancelButtonView = this._createButton( Drupal.t('Cancel'), icons.cancel, 'ck-button-cancel' );

		// Delegate ButtonView#execute to FormView#cancel.
		this.cancelButtonView.delegate( 'execute' ).to( this, 'cancel' );

    // Create a remove button to remove the span tag.
    this.removeButtonView = this._createButton( 'Remove', icons.eraser, 'ck-button-remove' );
    this.removeButtonView.delegate( 'execute' ).to( this, 'remove' );

		this.childViews = this.createCollection( [
			this.infoCardHintInputView,
      this.infoCardDataContentInputView,
			this.saveButtonView,
			this.cancelButtonView,
      this.removeButtonView
		] );

		this._focusCycler = new FocusCycler( {
			focusables: this.childViews,
			focusTracker: this.focusTracker,
			keystrokeHandler: this.keystrokes,
			actions: {
				// Navigate form fields backwards using the Shift + Tab keystroke.
				focusPrevious: 'shift + tab',

				// Navigate form fields forwards using the Tab key.
				focusNext: 'tab'
			}
		} );

		this.setTemplate( {
			tag: 'form',
			attributes: {
				class: [ 'ck', 'ck-infocard-form' ],
				tabindex: '-1'
			},
			children: this.childViews
		} );
    Drupal.editorAttach(this.infoCardDataContentInputView, 'Infocard');

  }

	render() {
		super.render();

		submitHandler( {
			view: this
		} );

		this.childViews._items.forEach( view => {
			// Register the view in the focus tracker.
			this.focusTracker.add( view.element );
		} );

		// Start listening for the keystrokes coming from #element.
		this.keystrokes.listenTo( this.element );
	}

	destroy() {
		super.destroy();

		this.focusTracker.destroy();
		this.keystrokes.destroy();
	}

	focus() {
		// If the infoCard text field is enabled, focus it straight away to allow the user to type.
		if ( this.infoCardHintInputView.isEnabled ) {
			this.infoCardHintInputView.focus();
		}
		// Focus the infoCard title field if the former is disabled.
		else {
			this.infoCardDataContentInputView.focus();
		}
	}

	_createInput( label ) {
		const labeledInput = new LabeledFieldView( this.locale,  createLabeledInputText );

		labeledInput.label = label;
    // console.log( labeledTextarea.fieldView );
    return labeledInput;
	}

	_createButton( label, icon, className ) {
		const button = new ButtonView();

		button.set( {
			label,
			icon,
			tooltip: true,
			class: className
		} );

		return button;
	}
}
