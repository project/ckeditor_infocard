import { Plugin } from 'ckeditor5/src/core';
import InfoCardEditing from './infocardediting';
import InfoCardUI from './infocardui';

export default class InfoCard extends Plugin {
	static get requires() {
		return [ InfoCardEditing, InfoCardUI ];
	}
}
