<?php

declare(strict_types=1);

namespace Drupal\Tests\ckeditor_infocard\FunctionalJavascript;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\ckeditor5\FunctionalJavascript\CKEditor5TestBase;
use Drupal\Tests\ckeditor5\Traits\CKEditor5TestTrait;
use Drupal\user\RoleInterface;

/**
 * Test the CKEditor InfoCard plugin.
 *
 * Inspired by \Drupal\Tests\ckeditor5\FunctionalJavascript\CKEditor5DialogTest.
 *
 * @group ckeditor_infocard
 */
class CKEditorInfoCardTest extends CKEditor5TestBase {
  use CKEditor5TestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ckeditor_infocard',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a filter format, and editor configuration with the abbreviation
    // plugin configured.
    FilterFormat::create([
      'format' => 'test_format',
      'name' => 'CKEditor InfoCard',
      'roles' => [RoleInterface::AUTHENTICATED_ID],
    ])->save();
    Editor::create([
      'format' => 'test_format',
      'editor' => 'ckeditor5',
      'settings' => [
        'toolbar' => ['items' => ['sourceEditing', 'span']],
        'plugins' => ['ckeditor5_sourceEditing' => ['allowed_tags' => []]],
      ],
    ])->save();
  }

  /**
   * Test that enabling the abbreviation plugin adds 'abbr' to the allowed tags.
   */
  public function testInfoCardPluginAllowsSpanTag(): void {
    $testTitle = 'Allows Abbreviation Tag Test';
    $expectedOutput = '<span data-content="Web Hypertext Application Technology Working Group">WHATWG</span>';

    // Edit a page, click CKEditor button for source editing, enter text with an
    // abbreviation, go back to the regular editing mode, ensure the
    // abbreviation button is now active, save, and verify the abbreviation is
    // visible on the saved page.
    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    $this->editSourceSetContent($expectedOutput);

    $this->assertEditorButtonEnabled('InfoCard');
    $this->assertTrue($this->getEditorButton('InfoCard')->hasClass('ck-on'));
    $this->assertStringContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput);
  }

  /**
   * Test that we can edit existing abbreviations.
   */
  public function testEditAbbreviation(): void {
    $testTitle = 'Edit Abbreviation';
    $expectedOutput1 = '<span data-content="Math Working Group">MWG</abbr>';
    $expectedOutput2 = '<span data-content="Media Working Group">MWG</abbr>';

    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);
    $this->waitForEditor();
    $this->editSourceSetContent($expectedOutput1);
    $this->assertStringContainsString($expectedOutput1, $this->getEditorDataAsHtmlString());
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput1);

    $node = $this->drupalGetNodeByTitle($testTitle);
    $this->drupalGet($node->toUrl('edit-form'));
    $this->waitForEditor();
    $this->assertStringContainsString($expectedOutput1, $this->getEditorDataAsHtmlString());

    // Change the abbreviation, and make sure it is saved.
    $this->assertEditorButtonEnabled('InfoCard');
    $this->pressEditorButton('InfoCard');
    [, $accordionText, $accordionDataContent, , $saveButton] = $this->checkAccordionBalloon();
    $this->assertStringContainsString('MWG', $accordionText->getValue());
    $accordionDataContent->setValue('Media Working Group');
    $saveButton->click();
    $this->assertStringContainsString($expectedOutput2, $this->getEditorDataAsHtmlString());
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been updated.");
    $this->assertSession()->responseContains($expectedOutput2);
  }

  /**
   * Test that we can add an abbreviation by selecting the abbreviation text.
   */
  public function testExistingTextActuallyAdd(): void {
    $testTitle = 'Actually Add Accordion To Existing Text';
    $expectedOutput = '<span title="Accessible Rich Internet Applications Working Group">ARIAWG</span>';

    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    $this->waitForEditor();
    $this->editSourceSetContent('The mission of the ARIAWG is to enhance the accessibility of web content.');

    $this->selectTextInsideEditor('ARIAWG');
    $this->assertEditorButtonEnabled('Accordion');
    $this->pressEditorButton('Accordion');

    [, $abbrText, $abbrTitle, , $saveButton] = $this->checkAccordionBalloon();
    $this->assertStringContainsString('ARIAWG', $abbrText->getValue());
    $abbrTitle->setValue('Accessible Rich Internet Applications Working Group');
    $saveButton->click();

    // Make sure the editor text contains the abbreviation we just created.
    $this->assertStringContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response contains the abbreviation we
    // just created.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()
      ->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput);
  }

  /**
   * Test that clicking the cancel button works correctly with text selected.
   */
  public function testExistingTextCancelAdd() {
    $testTitle = 'Cancel Adding Abbreviation To Existing Text';
    $expectedOutput = '<span data-content="Authoring Tool Accessibility Guidelines Working Group">ATAGWG</span>';

    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('title[0][value]', $testTitle);

    $this->waitForEditor();
    $this->editSourceSetContent('The ATAGWG was chartered to maintain and support the Authoring Tool Accessibility Guidelines.');

    $this->selectTextInsideEditor('ATAGWG');
    $this->assertEditorButtonEnabled('Abbreviation');
    $this->pressEditorButton('Abbreviation');

    [, $abbrText, $abbrTitle, $cancelButton] = $this->checkAccordionBalloon();
    $this->assertStringContainsString('ATAGWG', $abbrText->getValue());
    $abbrTitle->setValue('Authoring Tool Accessibility Guidelines Working Group');
    $cancelButton->click();

    // Make sure the editor text does not contain the abbreviation we just
    // cancelled.
    $this->assertStringNotContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response does not contain the
    // abbreviation we just cancelled.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()
      ->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseNotContains($expectedOutput);
  }

  /**
   * Test that we can add an abbreviation with no title through text selection.
   */
  public function testExistingTextNoTitle(): void {
    $testTitle = 'Add Abbreviation With No Title To Existing Text';
    $expectedOutput = '<span>JSON-LD</span>';

    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    $this->waitForEditor();
    $this->editSourceSetContent('The mission of the JSON-LD Working Group is to maintain the family of Recommendations and related Working Group Notes.');

    $this->selectTextInsideEditor('JSON-LD');
    $this->assertEditorButtonEnabled('Abbreviation');
    $this->pressEditorButton('Abbreviation');

    [, $abbrText, $abbrTitle, , $saveButton] = $this->checkAccordionBalloon();
    $this->assertStringContainsString('JSON-LD', $abbrText->getValue());
    $abbrTitle->setValue('');
    $saveButton->click();

    // Make sure the editor text contains the abbreviation we just created.
    $this->assertStringContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response contains the abbreviation we
    // just created.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()
      ->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput);
  }

  /**
   * Test that we can add an abbreviation without selecting text.
   */
  public function testNewAbbreviationActuallyAdd(): void {
    $testTitle = 'Actually Add New Abbreviation Test';
    $expectedOutput = '<span data-content="World Wide Web Consortium">W3C</span>';

    // Load a node/edit page and set a title.
    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    // Try adding an abbreviation, i.e.: without selecting text first.
    $this->waitForEditor();
    $this->assertEditorButtonEnabled('InfoCard');
    $this->pressEditorButton('InfoCard');

    // Check the abbreviation balloon, then set abbreviation text and title, and
    // click the balloon's Save button.
    [, $accordionText, $accordionDataContent, , $saveButton] = $this->checkAccordionBalloon();
    $accordionText->setValue('W3C');
    $accordionDataContent->setValue('World Wide Web Consortium');
    $saveButton->click();

    // Make sure the editor text contains the abbreviation we just created.
    $this->assertStringContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response contains the abbreviation we
    // just created.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput);
  }

  /**
   * Test that clicking the cancel button works correctly without text selected.
   */
  public function testNewAbbreviationCancelAdd(): void {
    $testTitle = 'Cancel Add New Abbreviation Test';
    $expectedOutput = '<span data-content="Web Applications Working Group">WAWG</span>';

    // Load a node/edit page and set a title.
    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    // Try adding an abbreviation, i.e.: without selecting text first.
    $this->waitForEditor();
    $this->assertEditorButtonEnabled('InfoCard');
    $this->pressEditorButton('InfoCard');

    // Check the abbreviation balloon, then set abbreviation text and title, and
    // click the balloon's Cancel button.
    [, $abbrText, $abbrTitle, $cancelButton] = $this->checkAccordionBalloon();
    $abbrText->setValue('WAWG');
    $abbrTitle->setValue('Web Applications Working Group');
    $cancelButton->click();

    // Make sure the editor text does not contain the abbreviation we just
    // cancelled.
    $this->assertStringNotContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response does not contain the
    // abbreviation we just cancelled.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseNotContains($expectedOutput);
  }

  /**
   * Test that we can add an abbreviation with no title, without selecting text.
   */
  public function testNewAbbreviationNoTitle(): void {
    $testTitle = 'Add New Abbreviation With No Title';
    $expectedOutput = '<span>PNG</span>';

    $this->drupalGet('node/add');
    $this->getSession()->getPage()
      ->fillField('data-content[0][value]', $testTitle);

    // Try adding an abbreviation, i.e.: without selecting text first.
    $this->waitForEditor();
    $this->assertEditorButtonEnabled('InfoCard');
    $this->pressEditorButton('InfoCard');

    [, $abbrText, $abbrTitle, , $saveButton] = $this->checkAccordionBalloon();
    $abbrText->setValue('PNG');
    $abbrTitle->setValue('');
    $saveButton->click();

    // Make sure the editor text contains the abbreviation we just created.
    $this->assertStringContainsString($expectedOutput, $this->getEditorDataAsHtmlString());

    // Save the page and make sure the response contains the abbreviation we
    // just created.
    $this->getSession()->getPage()
      ->pressButton('Save');
    $this->assertSession()->statusMessageContains("page $testTitle has been created.");
    $this->assertSession()->responseContains($expectedOutput);
  }

  /**
   * Check that the abbreviation prompt balloon appears with correct controls.
   *
   * @return \Behat\Mink\Element\NodeElement[]
   *   An array of five NodeElements in the following order, suitable for Array
   *   Unpacking / Array Destructuring (i.e.: with list() or [...]):
   *   1. the Balloon containing all the controls;
   *   2. the Abbreviation Text field;
   *   3. the Abbreviation Title field;
   *   4. the Balloon's Cancel button; and;
   *   5. the Balloon's Save button.
   */
  protected function checkAccordionBalloon(): array {
    $balloon = $this->assertVisibleBalloon('-span-form');

    $abbrText = $balloon->findField('Add InfoCard');
    $this->assertNotEmpty($abbrText);

    $abbrTitle = $balloon->findField('Add text');
    $this->assertNotEmpty($abbrTitle);

    $cancelButton = $this->getBalloonButton('Cancel');
    $this->assertNotEmpty($cancelButton);

    $saveButton = $this->getBalloonButton('Save');
    $this->assertNotEmpty($saveButton);

    return [$balloon, $abbrText, $abbrTitle, $cancelButton, $saveButton];
  }

  /**
   * Edit a CKEditor field's source, setting it to the given content.
   *
   * @param string $content
   *   The content to place in the CKEditor field.
   */
  protected function editSourceSetContent(string $content): void {
    $this->waitForEditor();
    $this->assertEditorButtonEnabled('Source');
    $this->pressEditorButton('Source');
    $this->assertEditorButtonDisabled('InfoCard');
    $sourceTextArea = $this->assertSession()
      ->waitForElement('css', '.ck-source-editing-area textarea');
    $sourceTextArea->setValue($content);
    $this->assertEditorButtonEnabled('Source');
    $this->pressEditorButton('Source');
    $this->assertEditorButtonEnabled('InfoCard');
  }

  /**
   * Find text inside the CKEditor, and select it.
   *
   * @param string $textToSelect
   *   The text to find and select.
   */
  protected function selectTextInsideEditor(string $textToSelect): void {
    $javascript = <<<JS
(function() {
  const textToSelect = "$textToSelect";
  const el = document.querySelector(".ck-editor__main .ck-editor__editable p");
  const startIndex = el.textContent.indexOf(textToSelect);
  const endIndex = startIndex + textToSelect.length;
  const range = document.createRange();
  const sel = window.getSelection();

  sel.removeAllRanges();
  range.setStart(el.firstChild, startIndex);
  range.setEnd(el.firstChild, endIndex);
  sel.addRange(range);
})();
JS;
    $this->getSession()->evaluateScript($javascript);
  }

}
